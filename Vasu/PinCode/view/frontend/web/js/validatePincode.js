define([
    "jquery"
], function($){
        "use strict";
        return function(config, element) {
        $(document).ready(function(){
            $('#txtpincode').focus(function(event){
                $('#checkpin').html('check').show();
            }
            );

            element.onclick = function()
            {
                console.log("hiii");
                let pin = $('#txtpincode').val();
                let len = pin.length;
                if(len != 6)
                {
                    $('#success').hide();
                    $('#displayerr').html("<b style='color:red'>* enter valid pincode</b>").show();
                }
                else if($('#checkpin').html()=='change')
                {
                    $('#txtpincode').focus();
                    console.log('in focus');
                }
                else
                {

                    $('#displayerr').hide();
                    $('#checkpin').attr('disabled',true);
                    $.ajax({
                        url:"pincode/ajax/checkajax",
                        type:"POST",
                        data:{
                            pincode:pin
                        },
                        beforeSend:function(){
                            $('#wait').css('display','');
                            $('#success').hide();
                        },
                        success:function(data){
                            console.log("in ajax");
                            if(data.status=="success")
                            {
                                let a="<p style='color:green'>"+data.message+"</p>";
                                $('#success').html(a).show();
                            }
                            else
                            {
                                let a="<p style='color:red'>"+data.message+"</p>";
                                $('#success').html(a).show();
                            }
                            $("#checkpin").html("change");
                            $('#checkpin').removeAttr('disabled');
                            $('#wait').css('display','none');
                        }

                        });
                

                    
                    console.log("bye");
                    // alert(len);
                }
                
            }
        });
        }
    }
)