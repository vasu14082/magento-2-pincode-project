<?php 
namespace Vasu\PinCode\Setup;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\InstallSchemaInterface;

class InstallSchema implements InstallSchemaInterface{
    public function install(SchemaSetupInterface $setup,ModuleContextInterface $context){
       
            $table = $setup->getConnection()
            ->newTable($setup->getTable('pincodes'))
            ->addColumn(
                'id',
                Table::TYPE_INTEGER,
                null,
                [
                    'identity' => true,
                    'unsigned' => true,
                    'nullable' => false,
                    'primary' => true
                ],
                'ID'
            )
            ->addColumn(
            'pincode',
            Table::TYPE_INTEGER,
            25,
            ['nullable' => false],
            'Pincode'
            )
            ->addColumn(
            'status',
            Table::TYPE_BOOLEAN,
            null,
            ['nullable' => false ],
            'Status'
            );
            $setup->getConnection()->createTable($table);
    }
}
 