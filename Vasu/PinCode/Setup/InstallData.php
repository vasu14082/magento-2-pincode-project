<?php
namespace Vasu\PinCode\Setup;

use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

/**
* @codeCoverageIgnore
*/
class InstallData implements InstallDataInterface
{
public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
{
	$data = [
		['pincode' => '400051',
		'status' => 1
		]
	];
	foreach ($data as $bind) {
	$setup->getConnection()->insertForce($setup->getTable('pincodes'), $bind);
	}
}
}