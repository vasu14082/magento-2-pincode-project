<?php
namespace Vasu\PinCode\Controller\Adminhtml\Helloworld;

class AddSave extends \Magento\Backend\App\Action
{

//const ADMIN_RESOURCE = 'Index';

protected $resultPageFactory;
protected $pincodeFactory;

public function __construct(
\Magento\Backend\App\Action\Context $context,
\Magento\Framework\View\Result\PageFactory $resultPageFactory,
\Vasu\PinCode\Model\PincodeFactory $pincodeFactory
)
{
$this->resultPageFactory = $resultPageFactory;
$this->pincodeFactory = $pincodeFactory;
parent::__construct($context);
}

public function execute()
{
$data = $this->getRequest()->getParam('pincode');

// var_dump($data);exit;
// if($data[])
$resultRedirect = $this->resultRedirectFactory->create();
$pincode = $data['pincode'];//echo strlen($pincode);exit;
$pinlen = (int)$pincode;
if(strlen($pincode)!=6 || strlen((string)($pinlen))!=6)
{
	$this->messageManager->addError('invalid PinCode');
	return $resultRedirect->setPath('*/*/addnew');
}
$pincoderecord = $this->pincodeFactory->create()->load($pincode);

if($data)
{
try{
$data = array_filter($data, function($value) {return $value !== ''; });

$pincoderecord->setData($data);
$pincoderecord->save();
$this->messageManager->addSuccess(__('Successfully saved the pincode.'));
$this->_objectManager->get('Magento\Backend\Model\Session')->setFormData(false);
return $resultRedirect->setPath('*/*/');
}
catch(\Exception $e)
{
$this->messageManager->addError($e->getMessage());
$this->_objectManager->get('Magento\Backend\Model\Session')->setFormData($data);
return $resultRedirect->setPath('*/*/addnew');
}
}

return $resultRedirect->setPath('*/*/');
}
}