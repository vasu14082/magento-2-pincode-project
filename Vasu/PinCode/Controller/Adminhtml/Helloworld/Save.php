<?php
namespace Vasu\PinCode\Controller\Adminhtml\Helloworld;

class Save extends \Magento\Backend\App\Action
{

    // const ADMIN_RESOURCE = 'Index';

    protected $resultPageFactory;
    protected $contactFactory;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Vasu\PinCode\Model\PincodeFactory $contactFactory
    )
    {
        $this->resultPageFactory = $resultPageFactory;
        $this->contactFactory = $contactFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $data = $this->getRequest()->getParam('pincode');
        // var_dump($data);exit;
        // echo "hiii";
        // exit;
        $resultRedirect = $this->resultRedirectFactory->create();

        $pinlen = (int)$data['pincode'];//echo $pinlen;exit;
        if(strlen($data['pincode'])!=6 || strlen((string)($pinlen))!=6)
        {
            $this->messageManager->addError('invalid PinCode');
            return $resultRedirect->setPath('*/*/edit');
        }

        $id = $data['id'];
        // echo $id;exit;
        $contact = $this->contactFactory->create()->load($id);
        // va($contact);
        // exit;

        if($data)
        {
            // $id = $data['pincode']['id'];
            // echo $id;exit;
            // $contact = $this->contactFactory->create()->load($id);
            try{
                // $contact = $this->contactFactory->create()->load($id);

                $data = array_filter($data, function($value) {return $value !== ''; });

                $contact->setData($data);
                $contact->save();
                $this->messageManager->addSuccess(__('Successfully saved the item.'));
                $this->_objectManager->get('Magento\Backend\Model\Session')->setFormData(false);
                return $resultRedirect->setPath('*/*/');
            }
            catch(\Exception $e)
            {
                $this->messageManager->addError($e->getMessage());
                $this->_objectManager->get('Magento\Backend\Model\Session')->setFormData($data);
                 return $resultRedirect->setPath('*/*/edit', ['id' => $contact->getId()]);
            }
        }

         return $resultRedirect->setPath('*/*/');
    }
}