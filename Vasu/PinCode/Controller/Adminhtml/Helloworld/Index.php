<?php

namespace Vasu\PinCode\Controller\Adminhtml\Helloworld;

class Index extends \Magento\Backend\App\Action
{
    protected $resultPageFactory;
    const ADMIN_RESOURCE = 'Index';

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    )
    {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
    }

    public function execute()
    {
        // print_r("expression");
        $resultPage = $this->resultPageFactory->create();
        // $resultPage->getConfig()->getTitle()->prepend((__('Posts')));

        return $resultPage;
    }


}






