<?php
namespace Vasu\PinCode\Controller\Adminhtml\Helloworld;
use Magento\Backend\App\Action;
use Vasu\PinCode\Model\Pincode;

class Edit extends \Magento\Backend\App\Action
{
    /**
     * Edit A Contact Page
     *
     * @return \Magento\Backend\Model\View\Result\Page|\Magento\Backend\Model\View\Result\Redirect
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function execute()
    {
        $this->_view->loadLayout();
        $this->_view->renderLayout();

        $contactDatas = $this->getRequest()->getParam('id');
        // echo $contactDatas;
        // exit;
        if(is_array($contactDatas)) {
            $contact = $this->_objectManager->create(Pincode::class);
            $contact->setData($contactDatas)->save();
            $resultRedirect = $this->resultRedirectFactory->create();
            return $resultRedirect->setPath('*/*/index');
        }
    }
}