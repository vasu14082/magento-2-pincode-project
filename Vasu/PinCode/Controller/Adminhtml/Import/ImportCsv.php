<?php 
namespace Vasu\PinCode\Controller\Adminhtml\Import;
use Magento\Framework\Controller\ResultFactory;
// use Magento\Framework\Setup\InstallDataInterface;
// use Magento\Framework\Setup\ModuleContextInterface;
// use Magento\Framework\Setup\ModuleDataSetupInterface;
use Vasu\PinCode\Model\ResourceModel\CollectionFactory;

 
class ImportCsv extends \Magento\Framework\App\Action\Action {
	protected $_pageFactory;
	protected $_categoryFactory;
	protected $_category;
	protected $_repository;
	protected $_pincodefactory;
	protected $csv;
	public function __construct(\Magento\Framework\App\Action\Context $context,
		\Magento\Framework\View\Result\PageFactory $pageFactory,
		// \Magento\Catalog\Model\CategoryFactory $categoryFactory,
		// \Magento\Catalog\Model\Category $category,
		\Magento\Framework\File\Csv $csv,
		\Magento\Catalog\Api\CategoryRepositoryInterface $repository,
		\Vasu\PinCode\Model\ResourceModel\CollectionFactory $pincodefactory
		){
		
		// echo "string construct";exit;
		$this->_pageFactory=$pageFactory;
		// $this->_categoryFactory=$categoryFactory;
		// $this->_category=$category;
		$this->_repository=$repository;
		$this->csv=$csv;
		$this->_pincodefactory=$pincodefactory;
		parent::__construct($context);
		
	}
	public function execute() {
		$insertCount=0;
		$errorCount=0;
		$rowErrorCount=0;
		
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$post=$this->getRequest()->getFiles();
		// if($post['csv_upload']['type']=='text/csv')
			if(isset($post['csv_upload']['tmp_name']) && ($post['csv_upload']['type']=='text/csv')) {
				$arrResult=$this->csv->getData($post['csv_upload']['tmp_name']);
				if(count($arrResult)==0)
				{
					$this->messageManager->addNotice('please enter Data ');
					return $this->_redirect('adminform/helloworld/import');
				}
				// echo "<pre>";print_r($arrResult);exit;
				if(($arrResult[0][0] != 'pincode') || ($arrResult[0][1]!='status') || (count($arrResult[0])!=2) || count($arrResult)==0)
				{
					$this->messageManager->addNotice('please check sample file of csv ');
					return $this->_redirect('adminform/helloworld/import');
				}
				else
				{
					if(count(array_keys($arrResult))==1)
					{
						$this->messageManager->addNotice('you dont have any data');
						return $this->_redirect('adminform/helloworld/import');
						// $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
      //   				$resultRedirect->setPath('*/*/import');
      //   				return $resultRedirect;
					}
					// print_r(count(array_keys($arrResult)));exit;
					foreach ($arrResult as $key=> $value) {
							if ($key == 0) { continue; }
							// echo count($value);exit;
							//if(count($value)!=2){$errorCount++;continue;}
							// var_dump(isset($value[1]);exit();
							if(is_string($value[0]))
							{
								// var_dump(isset($value[1]));exit;
								$pin = (int)$value[0];
								
								// echo strlen($value[1]);exit;

								// if(isset($value[1]) == false){$errorCount++;continue;}
								// echo (int)$value[0];exit();
								if(count($value)!=2 || strlen($value[0])!=6 || strlen((string)$pin)!=6 ) {$rowErrorCount++;echo $rowErrorCount;continue;}
								
								$status = (int)$value[1];
								if(!($status<=0))
								{
									// echo $status;exit;
									$rowErrorCount++;continue;
								}
								// if($status!=0 || $status!=1){$rowErrorCount++;continue;}
								// var_dump($status);exit;
								$pincode = $this->_pincodefactory->create();
								$check = $pincode->addFieldToFilter('pincode',['eq'=>$pin]);//->addFieldToFilter('status',['in'=>$status]);
								if(count($check->getData()) > 0) 
								{
									$errorCount++;
									continue;
									// goto end;
								}
								else
								{
									$model = $objectManager->create('Vasu\PinCode\Model\Pincode');
									$data =[
										'pincode'=>$pin,
										'status' =>$status
									];
									$model->setData($data);
									$model->save();
									$insertCount++;
								}
								
							}
						
					}

					if($rowErrorCount!=0){$this->messageManager->addError($rowErrorCount.' have incorrect format');}

					if($errorCount!=0){$this->messageManager->addNotice($errorCount.' have duplicate values');}

					if($insertCount!=0){$this->messageManager->addSuccess('you have entered '.$insertCount);}//.' rows, '.$errorCount.' rows already have in database and '.$rowErrorCount.' rows have incorrect format');
					
					
					return $this->_redirect('adminform/helloworld/import');
				}
				
			}
		else
		{
			$this->messageManager->addError('file type');
			return $this->_redirect('adminform/helloworld/import');
		}
	}	
}	
