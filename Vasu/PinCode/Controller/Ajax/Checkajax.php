<?php
namespace Vasu\PinCode\Controller\Ajax;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Vasu\PinCode\Model\PincodeFactory;
use Magento\Framework\Controller\Result\JsonFactory;
//use Magento\Framework\View\Result\PageFactory;
//use Magento\Framework\Session\SessionManagerInterface;

class Checkajax extends Action
{

  protected $_modelPincodeFactory;
  
  protected $jsonfactory;
 // protected $resultPageFactory;
 // protected $_sessionManager;

  public function __construct(
      Context $context,
      PincodeFactory $modelPincodeFactory,
      JsonFactory $jsonfactory
      //PageFactory  $resultPageFactory,
      //SessionManagerInterface $sessionManager
  )
  {
      
      $this->_modelPincodeFactory = $modelPincodeFactory;
      $this->jsonfactory = $jsonfactory;
      parent::__construct($context);
     // $this->resultPageFactory = $resultPageFactory;
     // $this->_sessionManager = $sessionManager;
  }

  public function execute()
  { 

    $status;
    $message; 
    $response;
    
  		// echo "hiii";exit;

       $pincode= $this->getRequest()->getPost('pincode');
       // echo ($pincode);
       // exit;
       // print_r($pincode);exit;
       $jsonfactory =$this->jsonfactory->create();
       $pincodedata = $this->_modelPincodeFactory->create();
       $pincodedata->load(intval($pincode),'pincode');
       // echo "string";


   if($pincodedata->getId())
   {
          if($pincodedata->getData()['status']== 1)
          {

           // echo "available";
            $status="success";
            $message="service is available";
            // $myJSON = json_encode($myObj);
            // return $myJSON;

           // echo "Service Is Available";
         
          }else
          {
            $status="fail";
            $message="service is not available";
      // $('#span_pinc1').html('<p>Avilable</p>');
          // return "Service Is Not Available";
          }
         
   }else
    {
      // return "Service Not Available";
        // $('#span_pinc1').html('<p>Avilable</p>');
            $status="fail";
            $message="service is out of range";
     }
     $response = ["status"=>$status,"message"=>$message];
     $jsonfactory = $jsonfactory->setData($response);
     return $jsonfactory;
}


}