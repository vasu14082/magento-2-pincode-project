<?php 
namespace Vasu\PinCode\Model;
class Pincode extends \Magento\Framework\Model\AbstractModel{
	const CACHE_TAG = 'pincodes';
	public function _construct(){
		$this->_init("Vasu\PinCode\Model\ResourceModel\Pincode");
	}

	public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }
}
 ?>