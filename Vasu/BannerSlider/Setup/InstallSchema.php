<?php 
namespace Vasu\BannerSlider\setup;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\InstallSchemaInterface;

class InstallSchema implements InstallSchemaInterface{
    public function install(SchemaSetupInterface $setup,ModuleContextInterface $context){
       
			$table = $setup->getConnection()
			->newTable($setup->getTable('contactData'))
			->addColumn(
			'name',
			Table::TYPE_TEXT,
			25,
			['nullable' => false],
			'Name'
			)
			->addColumn(
			'email',
			Table::TYPE_TEXT,
			255,
			['nullable' => false ],
			'Email'
			)
			->addColumn(
			'contact',
			Table::TYPE_NUMERIC,
			255,
			['nullable' => false,'unsigned' => true],
			'Contact'
			);
			$setup->getConnection()->createTable($table);
    }
}
 