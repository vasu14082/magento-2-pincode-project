<?php 
namespace Vasu\BannerSlider\Setup;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\DB\Ddl\Table;
class UpgradeSchema implements \Magento\Framework\Setup\UpgradeSchemaInterface{
 
	public function upgrade(SchemaSetupInterface $setup,ModuleContextInterface $context){
        $setup->startSetup();
        if (version_compare($context->getVersion(), '1.0.2', '<')) {
            $this->addStatus($setup);
        }
        $setup->endSetup();
	}
	
    /**
     * @param SchemaSetupInterface $setup
     * @return void
     */
    private function addStatus(SchemaSetupInterface $setup)
    {
        
        $setup->getConnection()->addColumn(
            $setup->getTable('contactData'),
            'id',
                [
                'type' => Table::TYPE_INTEGER,
                'nullable' => false,
                'identity'=>true,
                'primary'=>true,
                'unsigned' => true,
                'comment' => 'index field'
                ]
        );
        $setup->getConnection()->addColumn(
            $setup->getTable('contactData'),
            'created_at',
                [
                'type' => Table::TYPE_TIMESTAMP,
                'nullable' => false,
                'default' => Table::TIMESTAMP_INIT,
                'comment' => 'created at field'
                ]
        );
    }
}
?>

