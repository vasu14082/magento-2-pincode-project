<?php
namespace Vasu\BannerSlider\Controller\Form;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\Action\Context;



class Form extends \Magento\Framework\App\Action\Action
{


public function __construct(Context $context, PageFactory $pageFactory)
{
$this->pageFactory = $pageFactory;

return parent::__construct($context);
}

public function execute()
{

	$page_object = $this->pageFactory->create();
	return $page_object;
}
}