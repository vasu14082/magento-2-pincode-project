<?php 
namespace Vasu\BannerSlider\Controller\Index;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Vasu\BannerSlider\Model\DataExampleFactory;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Session\SessionManagerInterface;

class Index extends Action
{
protected $_modelDataExampleFactory;
protected $resultPageFactory;
protected $_sessionManager;

public function __construct(
Context $context,
DataExampleFactory $modelDataExampleFactory,
PageFactory $resultPageFactory,
SessionManagerInterface $sessionManager
)
{
parent::__construct($context);
$this->_modelDataExampleFactory = $modelDataExampleFactory;
$this->resultPageFactory = $resultPageFactory;
$this->_sessionManager = $sessionManager;
}

public function execute()
{
$resultRedirect = $this->resultRedirectFactory->create();
$DataExampleModel = $this->_modelDataExampleFactory->create();
$data = $this->getRequest()->getPost();

$DataExampleModel->setData('name', $data['name']);
$DataExampleModel->setData('email', $data['email']);
$DataExampleModel->setData('contact', $data['contact']);

$DataExampleModel->save();

$this->_redirect('schema/form/form');
$this->messageManager->addSuccess(__('The data has been saved.'));
}
}